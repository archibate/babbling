import io
import wave

def pcm2wav(pcm, framerate=16000, nchannels=1, sampwidth=2):
    with io.BytesIO() as f:
        with wave.open(f, "wb") as wf:
            wf.setframerate(framerate)
            wf.setnchannels(nchannels)
            wf.setsampwidth(sampwidth)
            wf.writeframes(pcm)
        wav = f.getvalue()
        return wav

def wav2pcm(wav):
    with io.BytesIO(wav) as f:
        with wave.open(f, "rb") as wf:
            framerate = wf.getframerate()
            nchannels = wf.getnchannels()
            sampwidth = wf.getsampwidth()
            frames = wf.readframes(wf.getnframes())
        return frames, framerate, nchannels, sampwidth
