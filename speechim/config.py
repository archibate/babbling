import os
import sys
import uuid

class Config:
    auth_app_id = int(os.environ['BAIDU_APP_ID'])
    auth_api_key = os.environ['BAIDU_API_KEY']
    auth_secret_key = os.environ['BAIDU_SECRET_KEY']
    auth_cuid = 'cu-{:x}'.format(uuid.getnode())
    auth_snid = uuid.uuid1().hex
    intent_memorize_time = 30
    intent_targets = {
        'input': dict(lookup=r'''
        都好,逗号=，
        冒号=：
        省略号=……
        等于,等于号,等号="="
        尖括号="<>"
        括号,圆括号="()"
        方括号="[]"
        花括号="{}"
        小于,小于号,左尖括号=">"
        大于,大于号,右尖括号="<"
        引号,双引号="\""
        单引号="'"
        空格=" "
        换行,换一行,回车="\n"
        '''),
        'assist': dict(title=r'^Chat App$', program=[sys.executable, '-m', 'gravityassist'], shortcut='F6', enter=True),
        'search': dict(title=r'Mozilla Firefox$', program='firefox', shortcut='Ctrl+T', enter=True),
        'open': dict(title=r'Mozilla Firefox$', program='firefox', shortcut='Ctrl+T', enter=True, lookup='''
        B站,比站,一战=https://space.bilibili.com
        收藏夹,收藏家,稍后再看,稍后在看=https://space.bilibili.com/263032155/favlist?fid=2793252855
        音乐,歌单,歌曲,播放列表=https://www.bilibili.com/medialist/play/ml1106514255
        百度=https://www.baidu.com
        必应=https://www.bing.com
        谷歌=https://www.google.com.hk
        百度控制台=https://console.bce.baidu.com/ai
        GitHub=https://www.github.com
        1969,一九六九,九号=https://live.bilibili.com/75287
        蓝冰,一块蓝冰,冰块,冰箱=https://live.bilibili.com/3092145
        '''),
        'note': dict(title=r'- Mousepad$', program='mousepad', enter=False),
        'terminal': dict(title=r'^终端 -', program='xfce4-terminal', enter=False),
    }
    intents = '''
    输入法,请输入,输入,请打字,打字=input
    选择,选中,点击,点按=choose
    小助手,小猪手=assist
    打开网页,请打开,打开,浏览,开=open
    谷歌搜索,搜索,谷歌,查找,收缩,收索,搜狐=search
    记笔记,笔记,记事本,记事,记录,第四本,日记=note
    输入命令,执行命令,命令,打开终端,终端,写代码,代码=terminal
    '''
