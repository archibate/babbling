import logging
from .stt import SpeechRecognizer
from .dispatch import TextDispatcher

logging.basicConfig(format='[%(asctime)-15s] [%(funcName)s()][%(levelname)s] %(message)s')
logging.getLogger().setLevel(logging.DEBUG)
td = TextDispatcher()
sr = SpeechRecognizer()
for text, finish in sr:
    td.input(text, finish)
