#!/bin/bash
set -e
cd "$(dirname "$0")"
mkdir -p autotts/models
test -f autotts/models/en_US-hfc_female-medium.onnx || curl -L https://huggingface.co/rhasspy/piper-voices/resolve/v1.0.0/en/en_US/hfc_female/medium/en_US-hfc_female-medium.onnx -o autotts/models/en_US-hfc_female-medium.onnx
test -f autotts/models/en_US-hfc_female-medium.onnx.json || curl -L https://huggingface.co/rhasspy/piper-voices/resolve/v1.0.0/en/en_US/hfc_female/medium/en_US-hfc_female-medium.onnx.json -o autotts/models/en_US-hfc_female-medium.onnx.json
test -f autotts/models/zh_CN-huayan-medium.onnx || curl -L https://huggingface.co/rhasspy/piper-voices/resolve/v1.0.0/zh/zh_CN/huayan/medium/zh_CN-huayan-medium.onnx -o autotts/models/zh_CN-huayan-medium.onnx
test -f autotts/models/zh_CN-huayan-medium.onnx.json || curl -L https://huggingface.co/rhasspy/piper-voices/resolve/v1.0.0/zh/zh_CN/huayan/medium/zh_CN-huayan-medium.onnx.json -o autotts/models/zh_CN-huayan-medium.onnx.json
test -f autotts/models/en_US-danny-low.onnx || curl -L https://huggingface.co/rhasspy/piper-voices/resolve/v1.0.0/en/en_US/danny/low/en_US-danny-low.onnx -o autotts/models/en_US-danny-low.onnx
test -f autotts/models/en_US-danny-low.onnx.json || curl -L https://huggingface.co/rhasspy/piper-voices/resolve/v1.0.0/en/en_US/danny/low/en_US-danny-low.onnx.json -o autotts/models/en_US-danny-low.onnx.json
test -f autotts/models/zh_CN-huayan-x_low.onnx || curl -L https://huggingface.co/rhasspy/piper-voices/resolve/v1.0.0/zh/zh_CN/huayan/x_low/zh_CN-huayan-x_low.onnx -o autotts/models/zh_CN-huayan-x_low.onnx
test -f autotts/models/zh_CN-huayan-x_low.onnx.json || curl -L https://huggingface.co/rhasspy/piper-voices/resolve/v1.0.0/zh/zh_CN/huayan/x_low/zh_CN-huayan-x_low.onnx.json -o autotts/models/zh_CN-huayan-x_low.onnx.json
